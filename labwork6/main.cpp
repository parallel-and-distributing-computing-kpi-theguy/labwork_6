/*------------------PaDC-----------------------------
---------------Labwork #6----------------------------
------------Threads in MPI---------------------------
--Group IO-52----------------------------------------
--Author: Butskiy Yuriy------------------------------
--Function 1 d = ((A*B) + (C*(B*(MA*MD))))-----------
--Function 2 MK = (TRANS(MA) * TRANS(MB * MM) + MX)--
--Function 3 T = (SORT(O + P) * TRANS(MR * MS)-------
--30.11.2017-----------------------------------------*/

#include <iostream>
#include "Data.h"
#include <Windows.h>
#include <mpi.h>

int N = 3;

int main(int argc, char* argv[])
{
	auto T1 = []() -> void
	{
		cout << "Task #1 started" << endl;
		Sleep(200);
		Vector A(N);
		Vector B(N);
		Vector C(N);
		Matrix MA(N, N);
		Matrix MD(N, N);
		Vector_Input(A);
		Vector_Input(B);
		Vector_Input(C);
		Matrix_Input(MA);
		Matrix_Input(MD);
		int d = F1(A, B, C, MA, MD);
		Sleep(200);
		cout << "Task #1 finished" << endl;
		if (N < 10)
			cout << "T1: d = " << d << endl;
	};

	auto T2 = []() -> void
	{
		cout << "Task #2 started" << endl;
		Sleep(200);
		Matrix MA(N, N);
		Matrix MB(N, N);
		Matrix MM(N, N);
		Matrix MX(N, N);
		Matrix_Input(MA);
		Matrix_Input(MB);
		Matrix_Input(MM);
		Matrix_Input(MX);
		Matrix MK = F2(MA, MB, MM, MX);
		Sleep(200);
		cout << "Task #2 finished" << endl;
		if (N < 10)
		{
			cout << "T2: MK = " << endl;
			Matrix_Output(MK);
		}
	};

	auto T3 = []() -> void
	{
		cout << "Task #3 started" << endl;
		Sleep(200);
		Vector O(N);
		Vector P(N);
		Matrix MR(N, N);
		Matrix MS(N, N);
		Vector_Input(O);
		Vector_Input(P);
		Matrix_Input(MR);
		Matrix_Input(MS);
		Vector T = F3(O, P, MR, MS);
		Sleep(200);
		cout << "Task #3 finished" << endl;
		if (N < 10)
		{
			cout << "T3: T = " << endl;
			Vector_Output(T);
		}
	};

	int rank;
	int processes;

	MPI_Init(&argc, &argv);
	cout << "Labwork #6 started" << endl;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &processes);

	switch (rank)
	{
	case 0:
		T1();
		break;
	case 1:
		T2();
		break;
	case 2:
		T3();
		break;
	default:
		cout << "Oops... No tasks available." << endl;
		break;
	}
	MPI_Finalize();

	cout << "Labwork finished" << endl;
	return 0;
}
